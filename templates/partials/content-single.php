<article <?php post_class('cnotv__single'); ?>>
  <header class="cnotv__single__header">
    <h1 class="cnotv__single__title"><?php the_title(); ?></h1>
  </header>
  <div class="entry-content">
    <?php the_content(); ?>
  </div>
  <div class="clearfix"></div>
  
  <footer>
  </footer>
</article>
